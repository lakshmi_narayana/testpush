var WebSocketClient = require('websocket').client;
var csv = require('csv-parser');
var fs = require('fs');
var path_to_csv = 'test3.csv';
var conf = require('./conf.json');
var counter = 0;

//function repeat() {
//    for (var a in conf) {
//        pushing(a, conf[a].name, conf[a].domain, conf[a].n, conf[a].port);
//    }
//}

var a = parseInt(process.argv[2]);

pushing(conf[a].name, conf[a].domain, conf[a].n, conf[a].port);

function pushing(name, domain, n, port) {
    var i = 0;
    var buffer = 10000;
    //var ip_add_to_connect = "127.0.0.1";
    var header;

    var client = new WebSocketClient();

    var uuid = require('node-uuid');
    client.on('connectFailed', function (error) {
        console.log('Connect Error: ' + error.toString());
        setTimeout(function () {
            client.connect('ws://127.0.0.1:' + port + '/', domain);
            console.log("retried successfully!!!");
        }, 1000);
    });

    client.on('connect', function (connection) {
        var conErrors = 0;
        console.log('WebSocket client connected');
        connection.on('error', function (error) {
            conErrors++;
            if (conErrors == 1) {
                console.log("Connection Error: " + error.toString());
                setTimeout(function () {
                    client.connect('ws://127.0.0.1:' + port + '/', domain);
                    console.log("retried- successfully!!!");
                }, 1000);
            }
        });
        connection.on('close', function () {
            console.log(domain + ' Connection Closed');
        });
        connection.on('message', function (message) {
            if (!message) {
                console.log("#############################null message from mta");
                process.exit(1);
            }
            if (message.type !== "utf8") {
                //console.log(a + "############################# new message type from mta:", message.type);
                //console.log(message);
                process.exit(1);
            }
            message = message.utf8Data;
            if ((message.indexOf('SUCCESS') === 0) || (message.indexOf('FAILED') === 0)) {
                console.log(message);
                return;
            }
            try {
                message = JSON.parse(message);
                //console.log(message);
                if (!message.type) {
                    console.log("################### invalid message.type from mta");
                    console.log(message);
                }
                if (message.type == "dl_rpt") {
                    handleDeliveryReports(message, function (err) {
                        if (err) {
                            console.log(err);
                            process.exit();
                        }
                    });
                }
            }
            catch (exp) {
                console.log(message);
                console.log(exp);
                process.exit(1);
            }
        });
        var text = {
            "subj": "subject!!!test",
            "fromname": "contact" + name,
            "msgid": "*IP_REPLACE_250*",
            "fromem": "contact@" + domain
        };
        text['body'] = "eJztXAtv2zgS/itTFznsLgJbluM4L+eQOr3bHDbdYJveYnE4FJREWWwkUUtSdny//mYoKVYcO7HdpHbbjQPHlkjOcObTvEjm5NX5r4PrP67eQmSSGK4+vPnlYgCNVuv3zqDVOr8+h5+vL3+BvabThmvFUi2MkCmLW6237xqnJ9QJ3zkLTk8SbhiOYjL+Zy5G/YYvU8NTYyYZb0D5pd8w/Na0qNsx+BFTmpv+WKSBHOu223Ubp3BihIn56eUE/vX+Chojwcfv+Fj/wo3hqvlJZw3Qhikj0iFkbMhPWkUHeMBAptgwYTXaqfSZH/HGnLb2BjVUMl6uB7/NhOK61tiZ1+yGT8ZSBfV25aX2bvnBrT505g0QcO0rkZHYa2NcR0ID/iYTKwRUxSs4iUV6A4rH/YY2k5jriHPTAFJAKXdfIx+R4mHVoklXTgHJtqwS4cSTwYSUwLyYg23UbyTsdiwCEx3tHTjZ7TFSDAIU/xHYb76MpToCNfR+aHc7u1C9/XgMIbKrxf/4EbRtU2SQR1wMI4NXml2eHIOH8+bqqJ3dgpaxCOB1GIbHOFMexyUdkqz9rjPmV98tQ/1G23F2GsBiMUxRiSgarhrgDS1P/QaNhT+NkkqpIlPNUdFbUM3yblZd5IXYbZxehDCROTDFIU+tRIwEQiQYkn/CRMzVCatGWCyJSuqk16NWq+32mg6+2kcHzsFha+xyGqq1s+MpfBC42tlBPgex8G8g4kj8pMVINeoUwR7Qm/1UTqNldTWrs5fXESytJITNy+kI/kANRWzEEfk+FyMe1JUDoZIJ/M4m7vvL9yBT8HjE4hBkCAM0ZR7DJ8bjPss1t6pGUyR8ZuwgzNhLY5nHAYrlxmq/JAIZjisLWwi8oKXvEWvCzzz1OfART0EUQLJs5qnOPXqmPaRie9xx4sskyVNiAAfetV0SNsHJizi+oyzD8B4tnegmlEgNJKTSoNx1VGdW534E85jczWLOcOrPiWE7Pzu9Asf+DI4tfasiRPpI+LwJS8B6SajtO49B7R/25wmojcruRmazQz0LN6vSjnlo6pM4PKMXWnyRDCutBUJnMZscebH0b6gnugj0k8pHusJMPoaoet0iDtGHDkU4tZ+oVfuw08e6eXmSo7VksWDyj1MJasx+QTFM6XYPDz4XCXOGWAEJ28LHAkS2zw875856vKxHvNC6Dfz6DTKfpbKLj/PQUPnA0t3U/F7bJbdHX0OWiHhydKYEi3fRfMcjbtAY74LGwBeNlaDAZBZN+FF8dNrNT9lwCifXmQKqd1hD1BMzelHOFzwOxQTcexPoHBx+bRPo3JvAfP4XeJiHVu9BdPLsZqa7mrWtccQ73OXul3zaPp94x3XXIK5IVFNapfSn6NmbhravHfuzApiW5dzpzvDzWSbrGRHUmQ+h55RSPRlo71O4/cj8yPbCv4VmcBVjAJvmCWUsA4ah6BuGEeBA8UAYGDAVrBRnPLuDd15cbo+JadZz3RcZzJVXzbst13yuGZ5jObHxjOdqLzacG4D4cqrq1HLc13uMXmtBvOM+AfEzIIkVqaEtFgkvN1xTqoXZl8JhR2g+9KuNCs1dTmjuMwnNfUpob5lKUW6IUw9x+hMl4VZYAaaLmNtmPA30LiRyJDC3zamup3FWJiLrYRuGSlAbYGmAzRRvNjcr3xVd96q+0q0ljaX7XaEyQiEVOFD8WV6hJQ4wO66FZ+5ikz+njHOw7zsEvZlSQjEmlR8D7ktlSxtHqUz5bEHB001bEED9T3TTl0nrTa404eFKZLzFgvd4F9Xi6b/7ad+Ef2NJduz3Xcd+SPy+LTTYL1ks+u3eYW+/0+3YC1dxfnHeL1qiMPv/MSLh2uDX/+JTnWUIxHdyTOWJJQPFNe4tGWGdn5+/OR+sjpre/vNHJotjbqoWczWb91gm7vxsd9WKwmfw4T7CR+/gy/HReUweB0vnIcvkKF/O4nVXzp+WAfp+57DjhV8oj1m3XLLXfWjytiP8by8X/rt7K6TYdTfwaAyrjZLp8PQSvTEozrRMbRDEyJKetMq7m43yX8pLr1y122t/vnnBHvctLSZ/cxLBqf6X1vg03zmoxYPWZ5dSIKaPHqwNufWVQE8aI5OjvXvhQiKCIOYzc+/uzISCRdyHbN1wA1mu/IhprrfWFC5IHr9eWLlbCKuHGKrm7DpP4YnFcZFeGB7zLMI4EzxaPcvYJEEDvsXI6nxjyOpsIbKWMlh3NfDeU2D7YESMc4Y3hLCrbUfY8mHwTOF16b0Yq5VKZzJXu6RfLOx/Zt5qkfFX3rodqHM730TWs0pmwxy2x3rTkTCJ+OJroy9fUNxkmlXLctavbi0uXFbTOEPOyz0+VFeHKyVGKLDhvQD1L4UsiCPah+6CkOU51NcunPeyvqm3UlH1OUS5NoE1wi1ajPj4IEns1XchOM9XxNkGQL+9vIBf7U44u8ooU3SxBnQks4x2BhfLClSTCGN+K3CKqxdoKz67ewsxsAEgdzvfA5Dd7wbI5yIlwNZcC+L5Q0bQdZ2dakkRmAGJEMdrjgOKYsBcsaWyjUWwPtibidY3ieoHzHybsO58N7D+Zyw9DJts5gLvuZ8rTsC+oLspqwdVtNYLg0hkZeOri3cIIwWYeSnMyagr8vly63LbIdPtYu7x3Gemsvswp3Z6+x6rLX3tr5EAddfcOj3D3JqljLazkj2aKWX0bCmj91cpY1NL8Kv4iRW93VQ17Vr98aBLr6X9XXd/a9cVV10s/+maq0SXthvj8CJj1sVaYBMu2a1I8mRaOKWjN7/pZhvDmAzjmQTFGRX2nvkR0NmboVQTamWXhvRurZJfEMnLqivV9enoS4ACFGF5aoY6XlcdWpd2CDB0cpH5JWOKIyOaB1TBvRwMNPxwyWndKaXdcyX5gQy4/pFWE6Sh00DepNh4R3uQLplGhZHvasJvfIh/KYCb7sSbLQxXSw+7tTZCYzOEPEkJOSccYrf6zO66AU+VjGNkgojTockhHU+S9tjQXekZTKRkPoyKq7+mVPne7D6pvRWBZI802v11aHjuNtVR4MtS/E1zDBhCzkv8dB3n65reA6NcmYx7Fng8HjdloTy/PBWGdrgp0lYmlWFxK+XjIU/tzk1aZWGYMLTS8cc3g2YWhI3T6aFBe9ZqelyRw6XUBi4SOw5CvXhsCVK1hxZlayo1fHPi1SvLl94TnnjI5lPStcAt2qLBqkTr34n265ImnARiVF36lGMIEE7w6hnOCu2SMgQUv9hwbCdemjFrgDMlfR5gvL8LY4682KOLf+ZC8WJhlg4lIv4ypjXJHMiZFm4U0GgbOVQsi44x8I9DfPgNxgicBJlZn2BHQP+KGbAmSniJ5EwzCYruIKw/QCta3A6kn1s72rQnRS0/Hrfbd9Ej4NCa44zQ7qM1Jjfkk631K1eCqtW8PsZ1VPsKOvcSYR0EiyU6AZvLEBzqAqlI3vDMTFvYA59EUFFMFtDeh0xqXuDGdqGTnB7trzG5SnlAxyRRK5vdVrNqdLAASBda54xOxxYirm9eJ9eINp+uaoluEblGpNX0UUiOpCSsAj6htuhRrG41bOyhJDKFGBX4MAq2FbLbX3tBKTig1+aPfG5YIovQRCEPZSsECELBsKg9aLQVBcREig9aUjyNzJO5sfBhvk9WhGrEIkU0CupTHEm2tmWElsRGfhahSGOIEVYGRZJgLaBMMpZigElmQdg+Qe6bVjmIDfKoRF2M4mEQ6qM5uXf2/N0ZMgcX6CUQo2eH4Drt/Rqti9RvThsT02fKh3OuUQI2kFV8KCgatXasoJowdXNHpDYMmhqBJouFqBnB6NhCrsugsjZKGUaSjGjaY6ni7bA87eV3oS4uhG7kIPGz7prdPLcP79VYKf9VRPFfJyq+gGoN8EpUsefiOuldhYH+Q8wCNpGwZeDE/huZ0/8DxYas0w==";
        //text['body'] = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" + "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" + "<head>\n" + "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />\n" + "<title>Untitled Document</title>\n" + "</head>\n" + " <body> \n" + "  <style type=\"text/css\">\n" + "div.preheader { display: none !important; }\n" + "</style> \n" + "  <div class=\"preheader\" style=\"font-size: 1px; display: none !important;\">\n" + "   The best network puts power in your hands. &#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;&#8204;&#160;\n" + "  </div> \n" + "   \n" + "   <table cellpadding=\"0\" border=\"0\" cellspacing=\"0\" width=\"636\" align=\"center\" style=\"table-layout:fixed; margin:0 auto;\"> \n" + "    <tbody>\n" + "     <tr> \n" + "      <td> <img src=\"images/tosavere_mailer_01.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></td> \n" + "     </tr> \n" + "     <tr> \n" + "      <td> \n" + "       <table cellpadding=\"0\" border=\"0\" cellspacing=\"0\" width=\"636\" align=\"center\"> \n" + "        <tbody>\n" + "         <tr> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjIyJnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_02.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjIzJnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_03.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjI0JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_04.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjI1JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_05.jpg\" style=\"width: 110px; height: 55px; display:block; border:0px;\"></a></td> \n" + "          <td><img src=\"images/tosavere_mailer_06.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></td> \n" + "         </tr> \n" + "        </tbody>\n" + "       </table> </td> \n" + "     </tr> \n" + "     <tr> \n" + "      <td> \n" + "       <table cellpadding=\"0\" border=\"0\" cellspacing=\"0\" width=\"636\" align=\"center\"> \n" + "        <tbody>\n" + "         <tr> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjI2JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_07.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td align=\"right\" width=\"349\"> <a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjI3JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_08.gif\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "         </tr> \n" + "        </tbody>\n" + "       </table> </td> \n" + "     </tr> \n" + "     <tr> \n" + "      <td><img src=\"images/tosavere_mailer_09.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></td> \n" + "     </tr> \n" + "     <tr> \n" + "      <td> \n" + "       <table cellpadding=\"0\" border=\"0\" cellspacing=\"0\" width=\"636\" align=\"center\"> \n" + "        <tbody>\n" + "         <tr> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjI5JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_10.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjMxJnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_11.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "         </tr> \n" + "        </tbody>\n" + "       </table> </td> \n" + "     </tr> \n" + "     <tr> \n" + "      <td> \n" + "       <table cellpadding=\"0\" border=\"0\" cellspacing=\"0\" width=\"636\" align=\"center\"> \n" + "        <tbody>\n" + "         <tr> \n" + "          <td><img src=\"images/tosavere_mailer_12.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjMzJnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_13.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "         </tr> \n" + "        </tbody>\n" + "       </table> </td> \n" + "     </tr> \n" + "     <tr> \n" + "      <td><img src=\"images/tosavere_mailer_14.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></td> \n" + "     </tr> \n" + "     <tr> \n" + "      <td> \n" + "       <table cellpadding=\"0\" border=\"0\" cellspacing=\"0\" width=\"636\" align=\"center\"> \n" + "        <tbody>\n" + "         <tr> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjM1JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_16.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjM2JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_17.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjM3JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_18.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjM4JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_19.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjM5JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_20.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjQwJnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_21.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><img src=\"images/tosavere_mailer_22.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></td> \n" + "         </tr> \n" + "        </tbody>\n" + "       </table> </td> \n" + "     </tr> \n" + "     <tr> \n" + "      <td> \n" + "       <table cellpadding=\"0\" border=\"0\" cellspacing=\"0\" width=\"636\" align=\"center\"> \n" + "        <tbody>\n" + "         <tr> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjQxJnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_23.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjQzJnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_24.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjQ0JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_25.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjQ2JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_26.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjQ3JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_27.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "          <td><img src=\"images/tosavere_mailer_28.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></td> \n" + "         </tr> \n" + "        </tbody>\n" + "       </table> </td> \n" + "     </tr> \n" + "     <tr> \n" + "      <td><img src=\"images/tosavere_mailer_29.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></td> \n" + "     </tr> \n" + "     <tr> \n" + "      <td><a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjQ4JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"outline:none; border:0px;\"><img src=\"images/tosavere_mailer_30.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></a></td> \n" + "     </tr> \n" + "     \n" + "     <tr> \n" + "      <td><img src=\"images/tosavere_mailer_31.jpg\" alt=\"\" align=\"absbottom\" border=\"0\" style=\"display:block;\"></td> \n" + "     </tr> \n" + "     <tr> \n" + "      <td valign=\"top\" align=\"left\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#959595; line-height:15px; padding-left:28px;\">  This email was sent to <a href=\"http://freshzeals.com/CLICK/click_a.php?u=\" target=\"_blank\" style=\"color:#959595; text-decoration:none; outline:none; border:0px;\"> <span style=\"color:#959595; text-decoration:none;\"> %%#EMAIL/%%</span></a>. <br> If you no longer wish to receive future emails, you may <a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NzE0JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%%%unsubscribe%%\" target=\"_blank\" style=\"color:#959595; outline:none; border:0px;\"><span style=\"color:#959595; text-decoration:underline;\">unsubscribe</span></a>, or&nbsp;easily adjust your subscription <br> preferences from your <a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjQ5JnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"color:#959595; outline:none; border:0px;\"><span style=\"color:#959595; text-decoration:underline;\">profile information</span></a>. </td>\n" + "     </tr> \n" + "     <tr> \n" + "      <td valign=\"top\" align=\"left\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#959595; padding-top:9px; line-height:15px; padding-left:28px;\"> We respect your privacy. Please review our <a href=\"http://freshzeals.com/CLICK/click_a.php?u=aHR0cDovL29yY2FsZWFwLmNvbS8/YT0xMzk3JmM9MTI5NjUwJnMxPTIyMjc2JnMyPVpBX1Byb21vIC8gU3VydmV5JnM0PVVTX09QX1kmczU9U1BfMjAxNjA3Mjg=_%%encval%%\" target=\"_blank\" style=\"color:#959595; outline:none; border:0px;\"><span style=\"color:#959595; text-decoration: underline;\">privacy policy</span></a> for more information about click activity with Verizon <br> Wireless and links included in this email. </td> \n" + "     </tr> \n" + "     <tr> \n" + "      <td valign=\"top\" align=\"left\" style=\"font-family:Arial, Helvetica, sans-serif; font-size:11px; color:#959595; padding-top:11px; line-height:15px; padding-left:28px;\"> Verizon Wireless, One Verizon Way, Mail <a href=\"http://freshzeals.com/CLICK/click_a.php?u=Iw==\" target=\"_blank\" style=\"text-decoration:none; color:#959595; outline:none; border:0px;\">Code: 180WVB, Basking Ridge, NJ 07920</a></td> \n" + "     </tr> \n" + "    </tbody>\n" + "   </table> \n" + "  \n" + " </body>\n" + "</html>";
        //text['body'] = "body!!!!hello";

        //var start_time = parseInt(Date.now() / 1000) + 60000;
        repeating(i);

        //text["em"] = "lakshmi101192@hroots.com";
        function repeating(i) {
            //if ((i < n) && (start_time > parseInt(Date.now() / 1000)) && buffer > 0) {
            if ((i < n) && buffer > 0) {
                fs.createReadStream(path_to_csv)
                    .pipe(csv())
                    .on('data', function (data) {
                        //console.log(a, data[header]);
                        text["tsid"] = uuid.v4();
                        text["em"] = data[header];
                        connection.sendUTF(JSON.stringify(text));
                        buffer--;
                        //console.log(a, JSON.stringify(text));
                    })
                    .on('headers', function (data) {
                        header = data[0];
                    })
                    .on("end", function () {
                        //console.log(a, "done");
                        setImmediate(function () {
                            //console.log(domain + " PUSHED " + (i + 1) + "     - - >  " + counter++);
                            repeating(i + 1);
                        });
                    });
            }
            else if (i === n) {
                console.log("*****************pushing completed");
            }
            else {
                setTimeout(function () {
                    repeating(i);
                }, 1000);
            }
        }
    });

    function handleDeliveryReports(msg, callback) {
        try {
            if (!msg.content) {
                return callback("content field not found in delivery report message");
            }
            msg = msg.content;
            if (!msg.result || !msg.tsid) {
                return callback("invalid delivery report message");
            }
            console.log(msg.tsid + "----" + msg.result);
            if (msg.result !== "deferred")
                buffer++;
            return callback(null);
            //console.log(msg.result);
            //console.log(msg.esp_resp);
        }
        catch (exp) {
            callback(exp.stack);
        }
    }

    client.connect('ws://127.0.0.1:' + port + '/', domain);
}

//repeat();